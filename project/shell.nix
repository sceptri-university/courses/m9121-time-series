let
    pkgs = import <nixpkgs> { };
in pkgs.mkShell {
    buildInputs = with pkgs; [
        quarto
        R
        texlive.combined.scheme-full
		pandoc
    ] ++ (with pkgs.rPackages; [
		quarto
		aod
		readxl
		forecast
		zoo
		rmarkdown
	]);
    shellHook = ''
    echo "Welcome to M9121 Project shell"
    '';
}
