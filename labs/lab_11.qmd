:::{.hidden}
{{< include mathematics.qmd >}}
:::

# Lab on 7th December 2023

## Exercise 1

### e)

The Wald test requested by the instructions has the statistics
$$
W = \vi \theta\Tr (\variance \vi \theta)^{-1} \vi \theta \sim \chi^2,
$$
where we test

- **Null hypothesis**: $\vi \theta = \vi 0$
- **Alternative**: $\vi \theta \neq \vi 0$

Or we can use likelihood ratio test, which uses
$$
L = 2(l(\vi \theta_B) - l(\vi \theta_s)) \sim \chi^2,
$$
where $\vi \theta_B$ are the coefficients of the **bigger** model and similarly for $\vi \theta_s$. Because we test 2 parameters in this case, we have 2 degrees of freedom.

## Exercise 2

The considered ARMA model
$$
	\overbrace{(1 - 0.4 \backs^{12})}^{\arPoly^*(\backs^{12})}\overbrace{(1 - 0.7\backs)}^{\arPoly(\backs)}X_t = \ve_t,
$$
which is $\SARIMA{1, 0, 0}{1, 0, 0}{12}$, can be re-written as follows
$$
X_t - 0.7 X_{t-1} - 0.4 X_{t-12} + 0.4 \cdot 0.7 X_{t-13} = \ve_t,
$$
which is technically $\ARmod{13}$.