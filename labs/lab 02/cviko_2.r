# via https://is.muni.cz/auth/el/sci/podzim2023/M9121/um/cviceni/ex02.html

##################
### Exercise 1 ###
##################

# a) generates 100 values of normal distribution
set.seed(1234)
x <- rnorm(100)
plot(x, type = 'o')

M <- matrix(0,100,20)
for (i in 1:20) {M[,i]<-rnorm(100)}
matplot(M, type = 'l')


# b)
M <- matrix(0,100,20)
for (i in 1:20) {M[,i]<-filter(rnorm(102), filter = c(1,0.5,0.2), method = 'convolution', sides = '1')[-(1:2)]}
matplot(M, type = 'l')


# d)
M <- matrix(0,100,20)
for (i in 1:20) {M[,i]<-(1:100)/10 +  filter(rnorm(102), filter = c(1,0.5,0.2), method = 'convolution', sides = '1')[-(1:2)]}
matplot(M, type = 'l')

# e)
M <- matrix(0,100,20)
for (i in 1:20) {M[,i]<-cumsum(filter(rnorm(102), filter = c(1,-0.6,0.3), method = 'convolution', sides = '1')[-(1:2)])}
matplot(M, type = 'l')



##################
### Exercise 2 ###
##################

set.seed(1)
N = 50
e = rnorm(N+1)
a1 = -0.8
a2 = 0.8
a3 = 0
x1 = (e[-1] + a1*e[1:N])/sqrt(1+a1^2)
x2 = (e[-1] + a2*e[1:N])/sqrt(1+a2^2)
x3 = (e[-1] + a2*e[1:N])/sqrt(1+a3^2)
mean1 <- cumsum(x1)/(1:50)
mean2 <- cumsum(x2)/(1:50)
mean3 <- cumsum(x3)/(1:50)
M <- cbind(mean1, mean2, mean3)
matplot(M, type = 'l')



##################
### Exercise 3 ###
##################

set.seed(5)
t = 0:50
x <- function(t, A = 1,f = 1/12, Phi = 0) {
  A * cos(2*pi*(f*t + Phi))
}


# a)
M <- matrix(0,51,20)
for (i in 1:20) {
  M[,i] <- x(t,1,1/12,runif(1))
}

matplot(M, type = 'l')

# b)
M <- matrix(0,51,20)
for (i in 1:20) {
  M[,i] <- x(t,rexp(1),1/12,runif(1))
}

matplot(M, type = 'l')

# c)
M <- matrix(0,51,20)
for (i in 1:20) {
  M[,i] <- x(t,rexp(1),1/12)
}

matplot(M, type = 'l')