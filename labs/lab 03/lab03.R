#install.packages("sandwich")
require(sandwich)
set.seed(1)
n <- 100

confInt <- function(x, method = "") {
  require(sandwich)
  
  x.avg <- mean(x)
  
  if (method == "lrvar") {
    x.sd <- sqrt(lrvar(x))
  } else {
    x.sd <- sqrt(var(x) / length(x) )
  }
  
  lower <- x.avg - 1.96 * x.sd
  upper <- x.avg + 1.96 * x.sd
  
  return (c(mu = x.avg, sd.mu = x.sd, lcl = lower, ucl = upper))
}

movingAverage <- function(x, coef) {
  filter(x, filter = coef, method = 'convolution', sides = '1')[-c(1:(length(coef) - 1))]
}

x<-movingAverage(rnorm(n+2), c(1, 0.5, 0.2))
confInt(x, method="lrvar")
confInt(x)

set.seed(1)
N <- 1000
xx <- replicate(N, movingAverage(rnorm(n+2), c(1, 0.5, 0.2)))

# We only want interval bounds
intervals <- apply(xx, 2, confInt, method = "lrvar")[c("lcl", "ucl"),]
mean(intervals[1,]<0 &intervals[2,]>0)


intervals.iid <- apply(xx, 2, confInt, method = "")[c(3,4),]
mean(intervals.iid[1,]<0 &intervals.iid[2,]>0)

set.seed(1)
N <- 1000
xx <- replicate(N, movingAverage(rnorm(n+2), c(1, -0.5, 0.2)))

# We only want interval bounds
intervals <- apply(xx, 2, confInt, method = "lrvar")[c("lcl", "ucl"),]
mean(intervals[1,]<0 &intervals[2,]>0)
intervals.iid <- apply(xx, 2, confInt, method = "")[c(3,4),]
mean(intervals.iid[1,]<0 &intervals.iid[2,]>0)


set.seed(12345)
n <- 100
e <- rnorm(n)
x <- (1:n) * e
y <- diff(x)
z <- e[-1]*e[-n] # we remove the first and the last element respectively

par(mfrow = c(1, 2))
plot(1:n, x)
plot(1:(n-1), y)

par(mfrow = c(1, 2))
plot(1:(n-1), z)
acf(z)

library(data.table)
dat = fread("https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/nakazeni-vyleceni-umrti-testy.csv")[,c(1,4)]
setnames(dat,c("date","deaths"))
setkey(dat,date)
dat[,deaths:=diff(c(0,deaths))]
x = dat[(date>=as.Date("2020-09-10"))&(date<=as.Date("2020-10-20")),ts(deaths)]

par(mfrow = c(1, 3))
plot(x)
plot(log(x))
plot(diff(log(x)))
