#For VsCode support
data(AirPassengers)
x = AirPassengers

x
str(x)
class(x)
plot(x)

start(x)
end(x)
frequency(x)
cycle(x)
time(x)
deltat(x)

x.mean <- aggregate(x, FUN=mean)
x.min <- aggregate(x, FUN=min)
x.max <- aggregate(x, FUN=max)

plot(x)
# column bind to matrix of aggregated values, then draw each column via matlines
matlines(as.vector(time(x.mean)) + .5, cbind(x.mean, x.min, x.max), type="o", pch=1)


x.range <- aggregate(x, FUN=function(u) {
  diff(range(u))
})
plot(x.range)


x.qmean <- aggregate(x, FUN=mean, nfrequency = 4)
x.qmin <- aggregate(x, FUN=min, nfrequency = 4)
x.qmax <- aggregate(x, FUN=max, nfrequency = 4)
plot(x)
matlines(as.vector(time(x.qmean)) + .125, cbind(x.qmean, x.qmin, x.qmax), type="o", pch=1)

quart <- matrix(x.qmean, ncol=4, byrow=1)
matplot(c(start(x)[1]:end(x)[1]), quart, type="o", pch=1)

matplot(t(quart), type="o", pch=1)

x.val <- as.numeric(x)
x.ts <- ts(x.val, start=c(1949, 1), end=c(1960, 12), frequency=12)
x.ts

set.seed(123)
sims <- 1000 #number of points

xy <- matrix(0, sims,2, dimnames =  list(NULL, c("x", "y")))
xy[,1] <- rnorm(sims)
xy[,2] <- sign(xy[,1]) * abs(rnorm(sims))
xy
