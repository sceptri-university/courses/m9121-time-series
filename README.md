# M9121 Time Series I

This book follows the course M9121 Time Series I taught by assoc. prof. Kraus on SCI MUNI, Department of Mathematics on Statistics, as it appeared in Fall 2023.

*The course offers a comprehensive coverage of selected fundamental methods and models for time series. The course covers theoretical foundations, statistical models and inference, software implementation, application and interpretation.*

*The students will gain a deeper understanding of the methods and their relations and learn to recognize situations that can be addressed by the models discussed in the course, choose an appropriate model, implement it and interpret the results.*

## Running

In the `shell.nix`, you shall necessary packages (and R packages) needed to render this Quarto project. If using Nix, you can run `nix-shell` to create a shell with all necessary packages. Then one can simply render the project using
```
quarto render --to all
```
in the current folder.

## Notice

I typesetted this entire material based on Kraus' presentation and as such I claim only the right to this material itself (which I distribute under the MIT license) and **not** the selection and style of presentation of the content.

## Contribution

Please, feel free to open an Issue or a Merge Request in case something seems wrong.