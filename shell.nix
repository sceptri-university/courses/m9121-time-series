let
  pkgs = import <nixpkgs> {};
  lib = import <lib> {};
  config = import <config> {};
  configPath = /home/sceptri/Documents/Dev/homelab/.;
  quartoPreRelease = extraRPackages:
    pkgs.callPackage (configPath + "/packages/quarto/preRelease.nix")
    (with pkgs lib config; {extraRPackages = extraRPackages;});

  rPackages = with pkgs.rPackages; [
    quarto
    aod
    readxl
    forecast
    zoo
    rmarkdown
    languageserver
    astsa
    TSA
    fma
  ];
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      texlive.combined.scheme-full
      pandoc
      xclip
      (
        rWrapper.override {
          packages = rPackages;
        }
      )
      (
        quartoPreRelease rPackages
      )
    ];
    shellHook = ''
      echo "Welcome to M9121 Project shell"
    '';
  }
